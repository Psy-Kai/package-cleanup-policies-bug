import os
from conan import ConanFile
from conan.tools.files import copy


class MyConanfile(ConanFile):
    name = "MyTestPackage"
    version = "0.0.0"
    author = "Kai Dohmen"
    options = {
        "A": [True, False],
        "B": [True, False],
        "C": [True, False],
        "D": [True, False],
        "E": [True, False],
        "F": [True, False],
    }
    default_options = {
        "A": True,
        "B": True,
        "C": True,
        "D": True,
        "E": True,
        "F": True,
    }

    def package(self):
        copy(self, pattern="foo.md", src=self.source_folder, dst=self.package_folder)
